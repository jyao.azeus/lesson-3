package com.example.lesson3.livedata

import android.os.Bundle
import android.util.Log
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.lesson3.R
import com.example.lesson3.databinding.LivedataActivityBinding
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch

class MainActivity : AppCompatActivity() {

    companion object {
        const val TAG = "MainActivity"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setTitle(R.string.livedata_activity_name)

        val binding = LivedataActivityBinding.inflate(layoutInflater)

        val viewModel by viewModels<LocalViewModel>()

        viewModel.numberCounterLiveData.observe(this) {
            binding.textView.text = it.toString()
            Log.i(TAG, "Textview value set to $it")
        }

        setContentView(binding.root)
    }
}

class LocalViewModel : ViewModel() {

    companion object {
        const val TAG = "MainActivity"
    }

    private val _numberCounterLiveData = MutableLiveData<Int>()
    val numberCounterLiveData: LiveData<Int> = _numberCounterLiveData

    init {
        viewModelScope.launch {
            while(true) {
                for (i in 1..100) {
                    delay(250)
                    _numberCounterLiveData.value = i
                    Log.i(TAG, "LiveData updated to $i")
                }
            }
        }
    }
}