package com.example.lesson3.service

import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.Service
import android.content.Intent
import android.os.Binder
import android.os.IBinder
import android.util.Log
import androidx.core.app.NotificationCompat
import androidx.core.app.NotificationManagerCompat
import com.example.lesson3.R

class LocalService: Service() {

    inner class LocalBinder : Binder() {
        fun getService(): LocalService = this@LocalService
    }

    companion object {
        val TAG = "LocalService"
        const val CHANNEL_ID = "CHANNEL_ID"
    }

    private val binder = LocalBinder()

    override fun onCreate() {
        super.onCreate()
        Log.i(TAG, "onCreate called")
    }

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        Log.i(TAG, "onStartCommand called")
        return START_NOT_STICKY
    }

    override fun onDestroy() {
        super.onDestroy()
        Log.i(TAG, "onDestroy called")
    }

    override fun onBind(intent: Intent?): IBinder {
        Log.i(TAG, "onBind called")
        return binder
    }

    override fun onUnbind(intent: Intent?): Boolean {
        Log.i(TAG, "onUnbind called")
        return false
    }

    fun raiseToForegound() {
        createChannel()

        val notif = NotificationCompat.Builder(this, CHANNEL_ID)
            .setSmallIcon(R.mipmap.ic_launcher)
            .setContentTitle("Service is running")
            .build()

        startForeground(1, notif)
    }

    fun stopForeground() {
        stopForeground(STOP_FOREGROUND_REMOVE)
    }

    private fun createChannel() {

        val notificationManager = NotificationManagerCompat.from(this)
        if (notificationManager.getNotificationChannel(CHANNEL_ID) == null) {
            val importance = NotificationManager.IMPORTANCE_DEFAULT
            val channel = NotificationChannel(
                CHANNEL_ID, "Local Service Channel", importance
            )
            channel.description = "Notifications by local service"
            notificationManager.createNotificationChannel(channel)
        }

    }
}