package com.example.lesson3.service

import android.content.ComponentName
import android.content.Context
import android.content.Intent
import android.content.ServiceConnection
import android.os.Bundle
import android.os.IBinder
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import com.example.lesson3.R
import com.example.lesson3.databinding.LocalServiceActivityBinding

class MainActivity : AppCompatActivity() {

    companion object {
        const val TAG = "MainActivity"
    }

    private lateinit var binding : LocalServiceActivityBinding

    private var mService: LocalService? = null
    private var mBound: Boolean = false

    /** Defines callbacks for service binding, passed to bindService()  */
    private val connection = object : ServiceConnection {

        override fun onServiceConnected(className: ComponentName, service: IBinder) {
            // We've bound to LocalService, cast the IBinder and get LocalService instance
            val binder = service as LocalService.LocalBinder
            mService = binder.getService()
            mBound = true

            Log.i(TAG, "Activity bound to service")
        }

        override fun onServiceDisconnected(arg0: ComponentName) {
            mService = null

            Log.i(TAG, "onServiceDisconnected called")
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setTitle(R.string.service_activity_name)

        binding = LocalServiceActivityBinding.inflate(layoutInflater)

        with (binding) {
            startService.setOnClickListener{ startService() }
            stopService.setOnClickListener{ stopService() }
            bindService.setOnClickListener{ bindService() }
            unbindService.setOnClickListener{ unbindService() }
            startForeground.setOnClickListener{ startForeground() }
            stopForeground.setOnClickListener{ stopForeground() }
        }

        setContentView(binding.root)
    }

    override fun onDestroy() {
        super.onDestroy()
        unbindService()
    }

    private fun startService() {
        Intent(this, LocalService::class.java).also { intent ->
            startService(intent)
        }

    }
    private fun stopService() {
        Intent(this, LocalService::class.java).also { intent ->
            stopService(intent)
        }
    }

    private fun bindService() {
        Intent(this, LocalService::class.java).also { intent ->
            bindService(intent, connection, Context.BIND_AUTO_CREATE)
        }
    }
    private fun unbindService() {
        if (mBound) {
            mBound = false
            unbindService(connection)
        }

    }
    private fun startForeground() {
        if (mBound) {
            mService?.raiseToForegound()
        }
    }
    private fun stopForeground() {
        if (mBound) {
            mService?.stopForeground()
        }
    }
}