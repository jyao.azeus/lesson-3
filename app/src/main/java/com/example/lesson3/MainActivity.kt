package com.example.lesson3

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.example.lesson3.databinding.ActivityListItemBinding
import com.example.lesson3.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {

    enum class Samples(val label: String, val activityClass: Class<out Any>) {
        HeadlessFragment("Headless Fragment", com.example.lesson3.headless.MainActivity::class.java),
        Service("Services", com.example.lesson3.service.MainActivity::class.java),
        LiveData("LiveData", com.example.lesson3.livedata.MainActivity::class.java),
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val binding = ActivityMainBinding.inflate(layoutInflater)

        Samples.values().forEach { sample ->
            val itemBinding = ActivityListItemBinding.inflate(layoutInflater)
            itemBinding.root.setOnClickListener {
                Intent(this, sample.activityClass).also { intent ->
                    startActivity(intent)
                }
            }
            itemBinding.text1.text = sample.label
            binding.list.addView(itemBinding.root)
        }

        setContentView(binding.root)
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        outState.putString("KEY", "From savestate")
    }
}