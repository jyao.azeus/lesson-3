package com.example.lesson3.headless

import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import androidx.lifecycle.LifecycleEventObserver

class HeadlessFragment : Fragment() {

    companion object {
        val TAG = HeadlessFragment::javaClass.name
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        lifecycle.addObserver(LifecycleEventObserver { _, event -> Log.i(TAG, "${hashCode().toString(16)}: ${event.targetState}") })
    }
}