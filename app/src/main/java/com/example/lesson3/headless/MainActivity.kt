package com.example.lesson3.headless

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.commitNow
import com.example.lesson3.R
import com.example.lesson3.databinding.HeadlessFragmentActivityBinding

class MainActivity : AppCompatActivity() {

    companion object {
        const val TAG_RETAINED_FRAGMENT = "RETAINED"
        const val TAG_UNRETAINED_FRAGMENT = "UNRETAINED"

        const val KEY_TEXT_CONTENT = "TEXT"
    }

    private val binding by lazy { HeadlessFragmentActivityBinding.inflate(layoutInflater) }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setTitle(R.string.headless_fragment_activity_name)
        setContentView(binding.root)

        if (savedInstanceState != null) {
            binding.text1.append(savedInstanceState.getString(KEY_TEXT_CONTENT))
        }

        if (supportFragmentManager.findFragmentByTag(TAG_RETAINED_FRAGMENT) == null) {
            HeadlessFragment().apply {
                retainInstance = true
            }.also {
                supportFragmentManager.commitNow { add(it, TAG_RETAINED_FRAGMENT) }
            }
        }

        if (supportFragmentManager.findFragmentByTag(TAG_UNRETAINED_FRAGMENT) == null) {
            supportFragmentManager.commitNow { add(HeadlessFragment(), TAG_UNRETAINED_FRAGMENT) }
        }


        binding.text1.append("""
            Retained fragment object: ${supportFragmentManager.findFragmentByTag(TAG_RETAINED_FRAGMENT)!!.toHex()}
            Unretained fragment object: ${supportFragmentManager.findFragmentByTag(TAG_UNRETAINED_FRAGMENT)!!.toHex()}
            
            
            """.trimIndent())

    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)

        outState.putString(KEY_TEXT_CONTENT, binding.text1.text?.toString())
    }

    fun Any.toHex(): String = hashCode().toString(16)
}